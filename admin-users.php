<?php

	use \Hcode\PageAdmin;
	use \Hcode\Model\User;

	$app->get('/admin/users', function() {
    
    User::verifyLogin();
    
    $users = User::listAll();
	
	$page = new PageAdmin();
	$page->setTpl("users",array(
		"users"=>$users
	));

});//fim app
	
	$app->get('/admin/users/create', function() {
    
    User::verifyLogin();
	
	$page = new PageAdmin();
	
	$page->setTpl("users-create");
	

});//fim app
	
	$app->get("/admin/users/:iduser/delete", function($iduser) {
	
	User::verifyLogin();
	
	$user = new User();
	
	$user->get((int)$iduser);
	
	$user->delete();
	header("Location: /admin/users");
	exit;

});//fim app
	
	$app->get("/admin/users/:iduser", function($iduser) {
	 
	 User::verifyLogin();
	
	 $user = new User();
	 
	 $user->get((int)$iduser);
	 
	 $page = new PageAdmin();
	 
	 $page->setTpl("users-update", array(
	  "user"=>$user->getValues()
	 ));

});//fim app
	
	$app->post("/admin/users/create", function() {
	
	User::verifyLogin();
	
	$user = new User();
	
	$_POST["inadmin"] = (isset($_POST["inadmin"])) ? 1 : 0;
	 $_POST['despassword'] = password_hash($_POST["password"], PASSWORD_DEFAULT, [
 	"cost"=>12
 	]);
	$user->setData($_POST);
	//var_dump($_POST);
	//exit;
	$user->save();
	header("Location: /admin/users");
	exit;
	//var_dump($_POST);

});//fim app
	
	$app->post("/admin/users/:iduser", function($iduser) {
	User::verifyLogin();
	
	$user = new User();
	$_POST["inadmin"] = (isset($_POST["inadmin"]))?1:0;
	
	$user->get((int)$iduser);
	
	$user->setData($_POST);
	
	$user->update();
	header("Location: /admin/users");
	exit;

});//fim app

?>