<?php

	use \Hcode\PageAdmin;
	use \Hcode\Model\User;

	$app->get('/admin', function() {
    
    User::verifyLogin();

    header("Location: /admin");
    
    //var_dump($_SESSION["User"]);
	//exit;
	$page = new PageAdmin();
	$page->setTpl("index");

});//fim app
	
	$app->get('/admin/login', function() {
    
	$page = new PageAdmin([
		"header"=>false,
		"footer"=>false
	]);
	$page->setTpl("login");

});//fim app
	
	$app->post('/admin/login', function() {
    
	User::login($_POST["login"],$_POST["password"]);
	header("Location: /admin");
	exit;
	$page->setTpl("login");

});//fim app

	$app->get('/admin/login',function() {
    if (User::verifyLogin()) {
    header("Location: /admin");
    exit;
    }
    $page = new PageAdmin([
    "header"=>false,
    "footer"=>false
    ]);
    $page->setTpl("login");

});//fim app
	
	$app->get('/admin/logout', function() {
    
	User::logout();
	header("Location: /admin");
	
	exit;
});//fim app

	$app->get("/admin/forgot", function() {
	
	$page = new PageAdmin([
	"header"=>false,
	"footer"=>false
]);
	$page->setTpl("forgot");

});//fim app
	
	$app->post("/admin/forgot", function(){
	$user = User::getForgot($_POST["email"]);
	
header("Location: /admin/forgot/sent");
exit;

});//fim app
	
	$app->get("/admin/forgot/sent", function() {
	
	$page = new PageAdmin([
	"header"=>false,
	"footer"=>false
]);
	
	$page->setTpl("forgot-sent");
	header("Refresh:5; url=/admin");

});//fim app
	
	$app->get("/admin/forgot/reset", function() {
	
	$user = User::validForgotDecrypt($_GET["code"]);
	
	$page = new PageAdmin([
	"header"=>false,
	"footer"=>false
]);
	
	$page->setTpl("forgot-reset", array(
		"name"=>$user["desperson"],
		"code"=>$_GET["code"]
	));

});//fim app
	
	$app->post("/admin/forgot/reset", function() {
	$forgot = User::validForgotDecrypt($_POST["code"]);
	User::setForgotUsed($forgot["idrecovery"]);	
	
	$user = new User();
	
	$user->get((int)$forgot["iduser"]);
	
	$user->setPassword($_POST["password"]);
	
	$page = new PageAdmin([
	"header"=>false,
	"footer"=>false
	]);
	
	$page->setTpl("forgot-reset-success");
	header("Refresh:5; url=/admin");
});//fim app

?>


